
# Hinführung \label{hinfuhr}

Zunächst werden wir uns intuitiv eine Sprache (bzw. eine Klasse von Sprachen) überlegen, für die es einen unberechenbaren Trade-off geben könnte. In \cref{proof} werden wir dann diese Sprache verwenden, um Theorem 1 zu beweisen. \

Welche Eigenschaften muss dieses Sprache haben? Zum einen muss sie die Unberechenbarkeit greifbar machen, und zum anderen muss sie durch E+A erkennbar sein, nicht jedoch durch EoA.

Wegen der Unberechenbarkeit ist klar, dass es keine einzelne Sprache geben kann, die diese Eigenschaften erfüllt, daher suchen wir zunächst eine Klasse von Sprachen $\mathbb L_a$ für die gilt:

$$ \small\forall f\in\mathbb F: \exists L_a \in \mathbb L_a:  \infty > |L_a| > f(s_+(L_a))$$

Wir brauchen also eine Klasse von Sprachen, in der es Sprachen gibt mit beliebig, aber endlich, vielen Wörtern, die von einem E+A mit endlich vielen Zuständen erkannt werden können. In \cref{valc} werden wir zeigen, dass die Sprache der validen Konfigurationen einer Turingmaschine $valc(M)$ eine solche Sprache ist. \

Um den Unterschied zwischen EoA und E+A greifbar zu machen, brauchen wir eine Sprache, die von einem E+A erkannt werden kann, jedoch nicht von einem EoA. Eine solche Sprache ist zum Beispiel:

$$L_b = \{w^{2^{2^{|w|}}}\mid w \in (\texttt \$\Sigma^+ \texttt \& ) \}$$

Ein E+A, der $L_b$ erkennt arbeitet wie folgt: Es werden zwei binäre Zähler verwendet. Einer zählt wie oft ein Teilwort wiederholt wurde. Der Andere zählt wie lang ein Teilwort ist. Bei beiden Zählern wird das niedrigste Bit zuerst geschickt und das höchstwertige Bit zuletzt. Wenn beim Hochzählen eine weitere Stelle für den Zähler benötigt wird, kann so einfach ohne Zeitverlust am Ende eine Stelle angehängt werden. Wenn beide Zähler in der ersten Zelle ankommen, wird geprüft, ob beide Zähler den gleichen Wert haben. Außerdem wird geprüft, dass beide Zähler an allen Stellen eine Eins haben, da es sich am Ende um eine 2er-Potenz handelt muss, und die erste Stelle sich selbst nicht mitzählt. Ein formaler Beweis für das Erkennen dieser Sprache mittels E+A befindet sich in @ca. \

Wenn wir nun einfach $\mathbb L_a$ verbinden mit $L_b$ erhalten wir

$$L_c = \{ w^{2^{2^{|w|}}}\mid w \in \texttt{\$} valc(M) \texttt{\&} \}$$

In \cref{proof} werden wir genau diese Sprache verwenden um Theorem 1 zu beweisen.

\clearpage
