
## Glossar \label{glossar}

### Allgemeine Definitionen

Diese Definitionen sollten bereits bekannt sein. Sie werden hier aufgezählt, um Mehrdeutigkeiten vorzubeugen

DFS
:	Diskreter Endlicher Automat
:	engl. Deterministic Finite Automaton


CA
:	Zellularer Automat
:	engl. Cellular Automaton

$\lambda$
: leeres Wort

$2^S$ oder $P(S)$
: Potenzmenge von $S$

$|A|$
: Länge des Wortes $A$

$|w|_\texttt X$
: Anzahl der Vorkommnisse von Symbol `X` in dem Wort `w`

### Spezielle Definitionen

Im weiteren werden in dieser Arbeit folgende Definitionen verwendet:

Einwegzellularautomat
: $\,$
: Ein 1D Zellularautomat, in denen Zellen nur den Zustand ihres Nachfolgers kennen.

Echtzeit plus log Einwegzellularautomat
: E+A
: Einwegzellularautomat, bei dem die Berechnung einer Eingabe der Länge $n$ nach $n + log(n)$ Schritten endet

Echtzeit Einwegzellularautomat
: EoA, Echtzeit "ohne log" Einwegzellularautomat
: Einwegzellularautomat, bei dem die Berechnung einer Eingabe der Länge $n$ nach $n$ Schritten endet

Beschreibungssystem

: $\mathfrak S$
: Menge von endlichen Deskriptoren

Deskriptor
: $D$
: Beschreibung der Formalen Sprache $L(D)$ in einem Beschreibungssystem

beschriebene Sprachen
: $\mathfrak L(\mathfrak S)$
: Menge der Sprachen, die mit dem System $\mathfrak S$ beschrieben werden können
: $:=\{L(D)\mid D\in \mathfrak S\}$

Menge von Deskriptoren, die $L$ beschreiben
: $\mathfrak S(L)$
: = $\{D\in \mathfrak S \mid L(D)=L\}$

Alphabet des Deskriptors
: $alph(D)$
: $L(D) \subseteq P(alph(D))$

Komplexitätsmaß
: $c$
: engl. complexity measure
: $\mathfrak S \rightarrow \mathbb N \\ D \mapsto n$

Länge der Kodierung von Deskriptoren
: $length(D)$
: ist ein $sn$-measure

Maß für die Komplexität von Deskriptoren
: $s$-Maß
: engl. $s$-measure
: steht in einem rekursiven Zusammenhang zu $length(D)$ unter Berücksichtigung von $alph(D)$
: $c$ ist $s$-Maß gdw. eine totale, berechenbare Funktion $g$ existiert mit $length(D) \leq g(c(d),|alph(D)|) \forall D\in \mathfrak S$
: Anm.: Das $|alph(D)|$ steht da, da das Alphabet irgendeinen Einfluss auf das Maß haben muss
: Anm.2: Es handelt sich nicht um ein formales Maß (z.B. im Sinne der Maßtheorie)

Aufzählbares Maß für Komplexität
: $sn$-Maß
: engl. $sn$-measure
: wie $s$-measure, nur das Descriptoren aufzählbar sind in Reihenfolger ihrer Größe in $c(D)$

Obere Schranke der Komplexitätsänderung
: Totale Funktio  $f:\mathbb N \rightarrow \mathbb N$
: Obere Schranke für die Änderung der Komplexität bei der Umwandlung eines Deskriptors von einem System in einen äquivalenten Descriptor eines anderen Systems
: Seien $\mathfrak S_1$ und $\mathfrak S_2$ descr. Systeme mit Komplexitätsmaße $c_1$ und $c_2$. $f$ ist eine obere Schranke für die Änderung der Komplexität bei der Umwandlung von Deskriptoren von $\mathfrak S_1$ zu $\mathfrak S_2$ gdw. $\forall D_1 \in \mathfrak S_1$ mit $L(D_1) \in \mathfrak L(\mathfrak S_2): \exists D_2\in\mathfrak S_2(L(D_1)): c_2(D_2)\leq f(c_1(D_1))$

Untere Schranke der Komplexitätsänderung
: totale Funktion $f:\mathbb N \rightarrow \mathbb N$
: Seien $\mathfrak S_1$ und $\mathfrak S_2$ descr. Systeme mit Komplexitätsmaße $c_1$ und $c_2$. $f$ ist eine unter Schranke für die Änderung der Komplexität bei der Umwandlung von Descriptoren von $\mathfrak S_1$ zu $\mathfrak S_2$ gdw. für endlich viele Descriptoren $D_1 \in \mathfrak S_1$ mit $L(D_1) \in \mathfrak L(\mathfrak S_2): \exists \text{ ein minimaler } D_2\in\mathfrak S_2(L(D_1)): c_2(D_2)\geq f(c_1(D_1))$

Trade-off:
: deutsch "Kompromiss"
: Änderung eines Komplexitätsmaßes einer Beschreibung beim Wechsel in ein anderes Beschreibungssystem. 

Nicht rekursiver Trade-off
: $\,$
: Es gibt keine rekursive Trade-off Funktion $f$

Rekursive Funktion
: $\,$
: Berechenbare Funktion

Nicht rekursive Funktion
: $\,$
: Nicht semi-entscheidbare Funktion
: Das Komplement einer semi-entscheidbaren Funktion
