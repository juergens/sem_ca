# Descriptional Complexity and Cellular Automata

You can find examples of the compiled documents  [here](https://gitlab.com/juergens/sem_ca/builds/artifacts/master/browse/build/?job=build).

you can find the latest presentation [here](https://gitlab.com/juergens/sem_ca/-/jobs/artifacts/master/file/pres/presentation/index.html?job=pres)

For technical information, see [CONTRIBUTING.md](CONTRIBUTING.md)

## beipiele ausführen

es gibt ein jupyter notebook für eingie Beispiele aus diesem Dokument. Das Notebook kann z.B. so ausgeführt werden:

    docker run --rm -it -p 8888:8888 -v $(pwd)/notebook.ipynb:/home/jovyan/notebook.ipynb jupyter/scipy-notebook  start-notebook.sh --NotebookApp.token=''

## inhalt

In dieser Arbeit wird gezeigt werden, dass der Trade-off zwischen Echtzeit OCA (EOCA) und Echtzeit+log OCA (E+OCA) nicht berechnbar ist.

Hierzu wird eine Klase von formalen Sprachen konstruirt, die von einem E+OCA mit berechenbar vielen Zuständen erkannt werden kann, für die ein EOCA jedoch unberechenbar viele Zustände braucht.

Um diese Klasse von Sprachen zu finden benötigen wird zwei Dinge: Wir müssen eine Sprache finden, die E+OCA "besser" erkennen können als EOCA. Und wir brauchen eine Sprache, die beide "gut" erkennen können mit unberechenbar langen Worten.

Die Sprache VALC(TM) für endliche TM hat unberechenbar Lange Worte.

Eine Sprache deren Wörter die Form "s^2^|s|!" (wiebei s ein Wort endlicher Länge ist) haben, können von einem E+OCA mit endlich vielen Zuständen erkannt werden. Die Anzahl der Zustände, die ein EOCA hierzu braucht ist abhängig von der |s|. In @blubb ist beschriebn, wie ein E+OCA arbeitet, der diese Sprache erkennt.

Wenn für s nun Wörte der Sprache VALC(TM) eingesetzt werden, erhalten wir eine Sprache L_m für die gilt:

L_m kann von einem E+OCA mit berechenbar vielen Zuständen erkannt werden und L_m kann nur von einem EOCA erkannt werden, der unberechenbar viele Zustände hat. L_m ist also genau unser Zeuge für einen unberechenbaren Trade-off.

Ein signifikanter Teil von L_m ist das erkennen von VALC(TM). In anderen Arbeiten ist bereits gezeigt, dass dies Theoretisch möglich ist. Für das bessere Verständnis werden wir in dieser Arbeit eine Funktion angeben, die eine TM abbildet auf einen EOCA, der VALC(TM) erkennt.
