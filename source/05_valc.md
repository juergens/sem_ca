
# Die Sprache der validen Konfigurationen einer Turingmaschine  \label{valc}

In diesem Kapitel werden wir die Sprache $valc(M)$ beschreiben, und wir werden eine Konstruktion angeben, mit der aus einer beliebigen Turingmaschine $M$ ein EoA  gebaut wird, der $valc(M)$ erkennt.

## Definition von $valc(M)$

Wir verwenden die Definition von $valc$ von @ais auf Seite 134 und 135. Diese lautet wie folgt:

> Sei $M = \langle Q, \Sigma, T, \delta, q_0 , B, F \rangle$ eine deterministische Turingmaschine mit  Zustandsmenge $Q$, Eingabesymbole $\Sigma$, Bandsymbole $T$, Zustandsüberführungsfunktion $\delta$, Startzustand $q_0$, Blanksymbol $B$ und  Endzustand $F$. Ohne Beschränkung der Allgemeinheit nehmen wir an, dass $M$ immer nach einer geraden Anzahl an Schritten zum Halten kommt, und dass $M$ immer hält sobald sie einen Endzustand erreicht, und dass $M$ mindestens zwei Schritte macht, und dass $M$ niemals Blank-Symbole ausgibt. Zu jedem Zeitpunkt während einer Berechnung kann $M$ vollständig beschrieben werden durch eine momentane Beschreibung (abgekürzt ID für "instantaneous description"). Dies ist eine Zeichenkette  $tqt'\in T^*QT^*$, in der gilt: $q$ ist der aktuelle Zustand von $M$, der beschriebene Bandinhalt ist die Zeichenkette $tt'$, und der Lesekopf ließt das erste Symbol von $t'$. Die initiale ID von $M$ für die Eingabe $x\in\Sigma^*$ ist $w_0=q_0x$. Eine ID ist akzeptierend, wenn sie in $T^*FT^*$ liegt. Die Menge $valc(M)$ der validen Berechnungen von $M$ besteht aus allen endlichen Zeichenketten der Form $w_0\#w_1^R\#w_2\#w_3^R\#\dots\#w_{2n-1}^R\#w_{2n}$, wobei gelte $\#\notin T\cup Q$, $w_i$ sind IDs von $M$, $w_0$ ist eine initiale ID, $w_{2n}$ ist eine akzeptierende (und daher haltende) ID, $w_{i+1}$ ist die Nachfolgekonfiguration von $w_i$ für $0 \leq i \leq 2n-1$.


\begin{figure}
\centering
\subfloat[]
{
    \includegraphics[width=\textwidth]{assets/valc0}
}
\caption{Ein Ausschnitt eines Wortes der Sprache $valc(M)$, wobei $M$ in diesem Beispiel die Beispielmaschine $M_B$ ist, die in \Cref{tmex} definiert wird.}
\label{fig:valc}
\end{figure}


In \Cref{fig:valc} ist ein Ausschnitt eines Wortes dieser Sprache zu sehen. Jedes Wort dieser Sprache ist eine eindeutige Folge von Berechnungen, die von einem Eingabewort zu einem akzeptierenden Endzustand führt. Da die Turingmaschine deterministisch ist, hat $valc(M)$ genau ein Wort pro akzeptierter Eingabe von $M$.

In $valc$ wurden nicht offensichtliche Anpassungen vorgenommen, um die Erkennung durch EoA zu ermöglichen: Dadurch, dass aufeinanderfolgende IDs in umgekehrter Reihenfolge sind, kann direkt an der Grenze zwischen den IDs (welche durch das $\#$ gekennzeichnet sind) damit begonnen werden, die Bänder zu vergleichen.

Ein EoA, der $valc$ erkennt, bekommt die Eingabe, in dem jedes Symbol aus dem Eingabewort auf den Zustand der entsprechenden Zelle abgebildet wird. Nach $n=|Eingabe|$ Schritten, wird die Ausgabe des EoA an der Zelle 0 abgelesen.


## Implementierung von Erkennung von $valc$

Um ein Gefühl für die Arbeitsweise von EoA zu entwickeln und um die Sprache $valc(M)$ besser zu verstehen, wurde eine konkrete Implementierung eines EoA entwickelt, der $valc(M)$ erkennt.

Für unsere Implementierung von $valc(M)$ machen wir eine zusätzliche Einschränkung, um die Implementierung zu erleichtern: Alle IDs haben die selbe Länge. Dies wird dadurch umgesetzt, dass auch unbeschriebene Felder in die ID aufgenommen werden, wenn diese während der Berechnung zu irgendeinem Zeitpunkt von $M$ gelesen werden. Durch diese Einschränkung wird die Implementierung vereinfacht, da man keine Ausnahmebehandlung mehr braucht für den Fall, dass die Turingmaschine den bisher beschriebenen Bereich verlässt.

Der Zustand unseres EoA ist ein Tupel, das aus 6 Werten besteht:

1. Das initiale Eingabesymbol an dieser Zelle
2. Das Signal\footnote{alle Signale haben die maximale Geschwindigkeit von einer Zelle pro Iteration und bewegen sich in die einzige mögliche Richtung}, in dem das Eingabe Symbol durchgereicht wird
3. Das letzte Signal, was an Postion 2 durchgereicht wurde
4. Entweder ein Zwischenzustand in der Auswertung, oder ein Signal, dass das Ergebnis der lokalen Auswertung symbolisiert für die Stelle, von den der das Signal an Position 2 losgeschickt wurde
5. Zwischenzustand, um in der ersten Zelle die Ausgabe zu generieren
6. Speichert, ob der letzte Zustand, der in der 2. Position vorbeikommen ist, ein Endzustand war


\begin{figure}
\centering
\subfloat[]
{
    \includegraphics[width=0.33\textwidth]{assets/raumzeit_full}
    \label{fig:raumzeit_full}
}
\subfloat[]
{
    \includegraphics[width=0.33\textwidth]{assets/raumzeit_zoom2}
    \label{fig:raumzeit_zoom}
}
\caption{ \protect\subref{fig:raumzeit_full} Das gesamte Raumzeitdiagramm für das Erkennen eines Wortes in $valc$ \protect\subref{fig:raumzeit_zoom} Ein Ausschnitt aus dem Raumzeitdiagramm; Die Farben richten sich nach dem 4. Wert des Tupels. Dieses Diagramm wurde erstellt mit dem Automat aus \Cref{implement}. Man beachte, dass der 6. Wert des Tupels hier nicht dargestellt.}
\label{fig:raumzeit}
\end{figure}

In \Cref{fig:raumzeit} ist das Raumzeitdiagramm unseres EoA zu sehen für die Bearbeitung des Wortes \texttt{\#A1234\-\#432A0\-\#00A34\-\#4A000\-\#0000A\-\#0B000\-\#00B00\-\#0B000\-\#00B00\-\#000B0\-\#B0000\-\#000C0\#} und die Turingmaschine $T_B$, welche in \Cref{tmex} definiert ist.


In der ersten Iteration soll das Eingabesymbol in dem ersten Wert des Tupels stehen. Die anderen Werte werden mit dem Blanksymbol `_` initialisiert.

Man sieht, dass die Implementierung der ersten 3 Werte des Tupels trivial ist.

Der 4. Wert des Tupels wird wie folgt erzeugt: Wenn in der ersten Iteration der erste Wert des Tupels eine Raute ist, wird ein Stern `*` geschrieben. Das Stern `*` symbolisiert "bis zu dieser Position ist alles in Ordnung". Dieses `*` wird dann als Signal weiter geschickt. Wenn eine Zelle in der letzten Iteration an dieser Position einen `*` hatte, aber in der nächsten Iteration kein `*`-Signal mehr von der Nachbarzelle kommt, dann muss hier eine Entscheidung getroffen werden, denn das Symbol, dass diese Zelle in dem 1. Wert des Tupels hat, entspricht nun dem selben Feld auf dem Band der Turingmaschine, wie der 2. Wert im Tupel der Nachbarzelle, nur mit dem Unterschied, dass das der Wert von der Nachbarzelle aus der Nachfolgekonfiguration kommt. Das liegt daran, dass wir die Berechnung an der Wortgrenze beginnen, und die Nachfolgekonfiguration in umgekehrter Reihenfolge steht. Wenn der Lesekopf in dieser Konfiguration nicht in der Nähe der betrachteten Zelle ist, dann wurde das Band an dieser Stelle nicht geändert. In diesem Fall muss die Zelle nur prüfen, ob das eigene Symbol im 1. Wert des Tupels identisch ist mit dem 2. Wert des Tupels der Nachbarzelle. Wenn das der Fall ist, wird ein weiteres `*`-Signal losgeschickt und die Zelle führt einen Warteschritt aus. Sollte dies nicht der Fall sein, dann könnte es sein, dass der Lesekopf in der Nähe ist. Da sich der Lesekopf bewegen kann, muss für ein paar Iterationen gewartet werden, bis alle notwendigen Informationen über das Signal in dem 2. Wert des Tupels vorbeigekommen sind. Um uns diese Informationen zu merken, verwenden wir den 3. Wert des Tupels. Aus dem 1. Wert der eigenen Zelle und der Nachbarzelle kann vorhergesagt werden, in welchen Zustand sich die Turingmaschine in der nächsten Konfiguration befinden sollte.  Sobald 2 Warteschritte vergangen sind, sind alle Informationen vorhanden, um festzustellen, ob das erwartete Verhalten der Turingmaschine eingetreten ist oder nicht, und das entsprechende Signal wird dann an der 3. Position weitergeschickt. Dabei muss beachtet werden, dass die Zelle zunächst nicht weiß, ob sie sich in einer geraden oder ungeraden Konfiguration befindet, und daher nicht weiß ober die aktuelle Konfiguration in umgekehrter Reihenfolge vorliegt. Das kann ebenfalls in diesem Schritt festgestellt werden: Wenn die aktuelle ID umgekehrt ist, dann ist die Bewegung des Lesekopfes genau das Gegenteil der erwarteten Bewegung des Lesekopfes und außerdem ist das Symbol links vom Lesekopf die Eingabe. Sollte irgendeines der Symbole anders als erwartet sein, gibt es einen Fehler.

Also insgesamt kann die Zelle zu diesem Zeitpunkt folgende Fragen beantworten: Wurde die Zustandsüberführungsfunktion der Turingmaschine von dieser Konfiguration in die nächste Konfiguration korrekt ausgeführt? In welche Richtung hat sich der Lesekopf bewegt? Ist die aktuelle Konfiguration in umgekehrter Reihenfolge? Gab es einen Fehler? Dementsprechend werden in der 3. Position des Zustands nun verschiedene Signale losgeschickt: ein `X` falls alles in Ordnung ist und die aktuelle Konfiguration in der richtigen Reihenfolge ist. Ein `Y` falls die Reihenfolge der Symbole umgekehrt ist. Ein `V` falls nicht festgestellt werden konnte, ob die Reihenfolge in Ordnung ist. Ein `F` falls eines der Symbole, die im 2. Wert des Tupels ankamen, nicht der Vorhersage entspricht.

In der 1. Zelle werden dann alle Signale ausgewertet, die ankommen. Insbesondere wird hier darauf geachtet, dass `X` und `Y` Signale immer abwechselnd ankommen. Wenn zuletzt ein `X` angekommen ist, wird eine `1` in den 5. Wert des Zustandes geschrieben und wenn ein `Y` angekommen ist eine `0`. Wenn ein `F` angekommen ist, dann geht die Zelle in einen Fehlerzustand und bleibt dort für immer.

Sei $n$ die Länge der Eingabe. Wenn der 5. Wert des Zustandes der ersten Zelle in der $n$-ten Iteration eine $0$ ist, und wenn der letzte Zustand ein Endzustand war, dann wurde das Wort akzeptiert.

\clearpage
