
# Contributing

This repository is based on [this boilerplate](https://gitlab.com/juergens/markdown-latex-boilerplate). On that repo you can find technical information on the build system.

## do whatver ever you want

If you have a quick fix, just make it on master. The CI will tell you if it works.

If you found a error, but don't have fix, or if you don't understand something, just open an issue here.

You can submit changs via merge-request; or you can request write-access; or you can open an issue and comment with you fix. I don't care, as long as the fix arrives here somehow.

## builing locally

For bigger changes I recommend to clone the repo and compiling it locally (`make pdf`).

you can auto-update with this simple bash on-liner (assuming ubuntu 18.04):

    evince build/example.pdf &; while inotifywait -e close_write source; do make pdf; done

This rebuilds the repo whenever you save a file, and it opens evince on the output, which refreshes whenever the pdf changes.

## running the build

	make all

After this the compiled document can be found in folder `build`

For more build targets, see `Makefile`

If you don't want a presentation or a HTML-Document or a PDF-Document, remove it from `_CONFIG.txt` and from the corresponding-job from the makefile


## CI

here is what to do, if the CI is broken for some reason

If you host your project on gitlab, CI should work automagically, assuming you have a runner. You just need to push your commits, and 2 minutes later the artefacts can be downloaded.

After cloning your repo, you need to define a runner to get going:

get token from: Setting-->
CI / CD Settings --> Runners --> Specific Runners --> Setup a specific Runner manually

Configure Runner with token from above:

	docker run --rm -t -i -v /srv/gitlab-runner-sem-ca/config:/etc/gitlab-runner --name gitlab-runner-sem-ca gitlab/gitlab-runner register

start runner

	docker run -d --restart always --name gitlab-runner-sem-ca -v /srv/gitlab-runner-sem-ca/config:/etc/gitlab-runner -v /var/run/docker.sock:/var/run/docker.sock gitlab/gitlab-runner:latest

view runner's logs

	docker logs gitlab-runner

## Tips and Tricks

### auto build and preview

	evince build/example.pdf & while inotifywait -e close_write source; do make pdf; done

What's happening here?

`evince` is a pdf reader, that automatically refreshes, when the document changes. `&` will push evince to the background. `inotifywait` is a program, that stops, when a file is changed, and `make pdf` rebuilds the pdf


### atom texteditor

* use `language-markdown` and not the default `language-gfm`. ([reason](https://github.com/atom/language-gfm/issues/117#issuecomment-159977414))
* use `linter-markdown`, `minimap`, `git-log`
