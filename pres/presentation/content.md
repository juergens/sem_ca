
### Beschreibungskomplexität und Einwegzellularautomaten

von Björn Jürgens

---

<!-- .slide: class="two-floating-elements" -->

### Einweg- und Echtzeit- Zellularautomaten

- **Einweg ZA**: Daten fließen nur in eine Richtung
- **Echtzeit ZA**: $t \leq n$
- **Echtzeit+log ZA**: $t \leq n+log(n)$
- **Echtzeit+log Einweg ZA**: $t\leq n+log(n)$ und Daten fließen nur in eine Richtung
- Abkürzungen

	- **EoA**: **E**chtzeit (**o**hne log) EZ**A**
	- **E+A**: **E**chtzeit **plus** log EZ**A**

![Normaler ZA](assets/ca2.png)

![Einweg ZA](assets/oca2.png)

Note:

<small>
- Unterklassen entstehen durch Einschränkungen
- "Einwegstraße"
- t: Anzahl Berechnungsschritte
- n: Länge der Eingabe
</small>

---

### Beschreibungskomplexität

**Theorem**

> Der Trade-off zwischen E+A und EoA ist nicht berechenbar

Dies ist äquivalent zu:

> für alle berechenbaren Funktionen $f\in\mathbb F$ gilt:
>
> `$\exists L:f(s_+(L)) < s_o(L)$`

<small>

* $L:$ Formale Sprache
* $\mathbb F:$ Menge berechenbarer Funktionen auf $\mathbb N$
* $s_o(L):$ Anzahl Zustände, die ein EoA mindestens benötigt um $L$ zu erkennen
* $s_{+}(L):$ Anzahl Zustände, die ein E+A mindestens benötigt um $L$ zu erkennen
* $s(L) = \infty \Leftrightarrow$ Sprache wird nicht erkannt

</small>

Note:

<small>

- Wie kompliziert ist es eine reguläre Sprache zu beschreiben in einem Formalen System?

- Bewertung durch:
	- Anzahl Zustände
	- Anzahl Regeln der Zustandsüberführungsfunktion
	- Länge der Kodierung des Systems

- bekannter Trade-off: exp. Trade-off zwischen DFA und NFA

- Es werden nur Sprachen betrechtet, die in beiden Systemen beschrieben werden können

**$f$ ist nicht berechenbar**
Oder in anderen Worten: n_a ist so groß, dass es keine turing machine gibt, die n_a aus n_b berechnen kann
</small>

--

<!-- .slide: class="nobull big_formular" -->


### Hinführung

1\. Wir suchen eine Klasse von Sprachen $\mathbb L_a$ für die gilt:


> `$$ \small\forall f\in\mathbb F: \exists L_a \in \mathbb L_a:  \infty > |L_a| > f(s_o(L_a))$$`

2\. Wie suchen eine Sprache $L_b$ für die gilt:

> `$$\small\begin{aligned}
s_+(L_b) &< \infty \\
 \wedge \ s_o(L_b) &=  \infty
\end{aligned}$$`

3\. $\mathbb L_a$ und $L_b$ werden wir dann zu $\mathbb L_c$ kombinieren womit wir die Behauptung beweisen werden.

Note:
* L_1: Eine Sprache, die mit der unberechenbarkeit greifbar wird. Wir suchen eine Klasse von Sprachen, sodass
* L_2: Eine Sprache, Die E+A erkennen können, aber EoA nicht.

---

<!-- .slide: class="big_formular" -->

#### Wie sieht $\mathbb L_a$ aus?

> `$$ \forall f: \exists L_a \in \mathbb L_a: \infty > |L_a| > f(s_o(L_a))$$`


**Frage: Welche Funktion wächst zu schnell um berechenbar zu sein?**

$$g: \mathbb N \rightarrow \mathbb N \\\
g(n) \mapsto ?$$

<p class="fragment fade-in">$g(n) \mapsto $ Anzahl Wörter, die eine Turingmaschine mit $n$ Zuständen erkennen kann</p>

<p class="fragment fade-in">
Idee: Wörter in $L_a$ sind Folgen valider Konfigurationen einer Turingmaschine</p>

Note: Dann habe ich eine Sprache mit schön vielen Wörten, die dann auch schön lang sind.

--

Idee: Wörter in $L_a$ sind Folgen valider Konfigurationen einer Turingmaschine

* Turingmaschine $M = \langle Q, \Sigma, T, δ, q_0 , B, F \rangle$
  * Zustandsmenge $Q$, Eingabesymbole $\Sigma$, Bandsymbole $T$, Zustandsüberführungsfunktion $\delta$, Startzustand $q_0$, Blank $B$, Endzustand $F$
* Konfiguration $w = tqt'\in T^\*QT^\*$ mit
	*  $tt'$ ist Band
	* Zustand ist $q$
	* Kopf steht auf erstem Symbol von $t'$
* $w_i:$ Konfiguration von $M$ nach $i$ Schritten
	* Startkonfiguration $w_0=q_0x$
	* $w\_i$ folgt durch Zustandsüberführung aus $w\_{i-1}$

`$$valc(M)= \{w_0\#w_1^R\#w_2\#w_3^R\#...\#w_{2n} \\\
\mid w_0=q_0x, x\in T^* \}$$`


$\mathbb L_a = \\{valc(M)\mid M\in \mathbb{DTM} \\}$

Note:

* obda: Hält nach gerade Anzahl Schrittes, druckt keine Blanks, macht mindestens 2 Schritte, Hält sobald Endzustand erreicht ist.

--

![valc(M_0)](assets/valc.png)


--

#### Wie sieht $L_b$ aus

Frage: Was können E+A was EoA nicht können?

Antwort: <span class="fragment fade-in">Zählen </span>

<p class="fragment fade-in">Idee: In der Sprache wird ein Teilwort $2^{2^{|w|}}$ mal wiederholt</p>

Note: Man muss nicht nur einen Zähler durchschicken, sondern am Ende auch wieder einlesen.

--

<!-- .slide: class="big_formular" -->
#### Wie sieht $L_b$ aus

> $$L_b = \\{w^{2^{2^{|w|}}}\mid w \in (\texttt \$\Sigma^+ \texttt & ) \\}$$

Erkennung von $L_b$ mit E+A:

* Durchschicken von 2 binären Zählern
	1. Zählt wie oft Wort wiederholt wurde
	2. Zähtt wie lang das Wort ist
* Niedrigstes Bit zuerst
* Am Ende überprüfen, ob
	* Beide Zähler identisch sind
	* Zähler nur aus 1en bestehen
* $\implies s_+(L_b) < \infty$


--

<!-- .slide: class="big_formular nobull" -->
#### Wie sieht $L_b$ aus

> $$L_b = \\{w^{2^{2^{|w|}}}\mid w \in (\texttt \$\Sigma^+ \texttt & ) \\}$$

Erkennung von $L_b$ mit EoA:

* Ein EoA kann keine $log(n)$ Schritte in das Lesen einer Zählvariable investieren

* $\implies$ EoA erkennt nur $L_b$, in dem alle Wörter fest in die Zustandsüberführungsfunktion einkodiert werden

* $\implies$ $s_o(L_b)$ ist abhängig von der Länge und Anzahl der Wörter in $L_b$
* $\implies s_o(L_b) = \infty$

Note:

blubb

--

<!-- .slide: class="nobull" -->

* `$\hphantom{\implies\,} L_a = valc(M)$`
* `$\hphantom{\implies\,} L_b = \{ w^{2^{2^{|w|}}}\mid w \in (\texttt \$\Sigma^+ \texttt & ) \}$`
* `$\implies L_c = \{ w^{2^{2^{|w|}}}\mid w \in \texttt{\$} valc(M) \texttt{&} \}$`


Mithilfe von $L\_c$ werden wir nun die Behauptung zeigen.

> Der Trade-off zwischen E+A und EoA ist nicht berechenbar


--

#### Implementierung eines EoA

Zunächst überzeugen wir uns davon, dass $valc(M)$ von von EoA und E+A erkannt werden können.

*Siehe Jupyter*

http://wotanii.de:1338/tree/notebook.ipynb?token=sem_ca_19

<small>
alternativ: omero.wotanii.de/notebooks/notebook.ipynb?token=sem_ca_19
</small>

Note:

- techniken, die OCA benutzen können
- Tafel:
	- Wort aus valc aufmalen
	- erklären, wie man es manuell validieren kann
	- erklären, wie ein ZA abstract arbeiten würde
	- dann ins notebook gehen und konkrete implementierung anschauen

--

#### Beispiel Turingmaschine

Turingmaschine $M_1 = \langle Q, \Sigma, T, δ, q_0 , B, F \rangle$

* Zustand $Q = \{A,B,C\}$
* Eingabesymbole $\Sigma=\{1,2,3,4,5\}$,
* Bandsymbol $T=\Sigma \cup \\{\square\\\}$,
* Startzustand $q_0=A$
* Blank $B=\square$
* Endzustand $F=C$
* Zustandsüberführungsfunktion $\delta$:
`$$\begin{align}
(A,s)\mapsto &(A,0,r) (s \in \Sigma) \\
(A,\square)\mapsto &(B,0,l) \\
(B,s)\mapsto &(B,0,l) (s \in \Sigma) \\
(B,\square)\mapsto &(C,\square,r) \\
(C,t)\mapsto &(C,t,s) (t \in T)
\end{align}$$`

---

<!-- .slide: class="fullwidthquote" -->


Theorem 1

> Let $S\_1$ and $S\_2$ be two descriptional systems for recursive languages such that any descriptor $D$ in $S\_1$ and $S\_2$ can effectively be converted into a Turing machine that decides $L(D)$, and let $c\_1$ be a measure for $S\_1$ and $c\_2$ be an sn-measure for $S\_2$. If there exists a descriptional system $S\_3$ and a property $P$ that is not semidecidable for descriptors from $S\_3$, such that, given an arbitrary $D\_3 \in S\_3$,

> (i) there exists an effective procedure to construct a descriptor $D\_1$ in $S\_1$, and

> (ii) $D\_1$ has an equivalent descriptor in $S\_2$ if and only if $D\_3$ does not have property $P$, then the trade-off between $S_1$ and $S\_2$ is non-recursive.

>then the trade-off between $S\_1$ and $S\_2$ is non-recursive

--

<!-- .slide: class="fullwidthquote" -->

Theorem 1 (de)

> Seien $S\_1$ und $S\_2$ zwei Beschreibungssysteme für berechenbare Sprachen, so dass beliebige Beschreibungen $D$ in $S\_1$ und $S\_2$ effektiv in eine Turingmaschine konvertiert werden können, die $L(D)$ beschreibt, und sei $c\_1$ ein Maß für $S\_1$ und $c\_2$ ein sn-Maß für $S\_2$. Wenn es ein Beschreibungssystem $S\_3$ gibt mit einer Eigenschaft $P$, die nicht semi-entscheidbar ist für Beschreibungen von $S\_3$, sodass für ein beliebiges $D\_3$ in $S\_3$ gilt,

> (i) existiert eine effektives Vorgehen zum Konstruieren einer Beschreibung $D\_1$ in $S\_1$, und

> (ii) $D\_1$ hat eine equivalente Beschreibung in $S\_2$  genau dann wenn $D\_3$ keine Eigenschaft $P$ hat

> , dann ist der Trade-off zwischen $S\_1$ und $S\_2$ nicht berechenbar.

--

<!-- .slide: class="nobull" -->

##### Wörter einsetzen

* $S\_1 \rightarrow$ E+A
* $S\_2 \rightarrow$  EoA
* $c\_1 \rightarrow$  Anzahl Zustände
* $c\_2 \rightarrow$  Anzahl Zustände
* $S\_3 \rightarrow$  Menge der Turingmaschinen
* $D\_3 \rightarrow$  beliebige TM
* $P \rightarrow$ Unendlichkeit der Sprache, die eine TM erkennt
* $D\_1 \rightarrow$  E+A, der $L\_c$ erkennt
* $D\_2 \rightarrow$  EoA, der $L\_c$ erkennt

--

	<!-- .slide: class="fullwidthquote" -->

	Theorem 1 (spezielle Formulierung)

> Seien _E+A_ und _EoA_ zwei Beschreibungssysteme für berechenbare Sprachen, so dass beliebige Beschreibungen $D$ in _E+A_ und _EoA_ effektiv in eine Turingmaschine konvertiert werden können, die $L(D)$ beschreibt, und sei _Anzahl Zustände_ ein Maß für _E+A_ und _Anzahl Zustände_ ein sn-Maß für _EoA_. Wenn es ein Beschreibungssystem _Turingmaschinen_ gibt mit einer Eigenschaft _Unendlichkeit_, die nicht semi-entscheidbar ist für Beschreibungen von Turingmaschinen, sodass für ein beliebige _Turingmaschine_ $D_3$ in _Turingmaschinen_ gilt,

> (i) existiert eine effektives Vorgehen zum Konstruieren einer Beschreibung $D\_1$ als _E+A_, und

> (ii) $D\_1$ hat eine equivalente Beschreibung als _EoA_ genau dann wenn $D\_3$ _nicht unendlich ist_

> , dann ist der Trade-off zwischen _E+A_ und _EoA_ nicht berechenbar.

--

Der erste Teil von Theorem 1 ist trivialerweise wahr. Übrig bleibt:

> \[wenn] für eine beliebige Turingmaschine $D\_3$ gilt,

> (i) es existiert eine effektives Vorgehen zum Konstruieren eines E+A $D\_1$, und

> (ii) $D\_1$ hat eine equivalente Beschreibung als EoA  genau dann wenn $D\_3$ nicht unendlich ist

> dann ist der Trade-off zwischen E+A und EoA nicht berechenbar.

$\Rightarrow$ wir brauchen also nur (i) und (ii) zeigen und sind fertig

--

Der E+A $D_1$ muss irgendwie abhängig sein von der TM $D_3$. Wir wählen $D_1$ mit

$$L'(D_1) =  \\{ w^{2^{2^{|w|}}}\mid w \in \texttt{\$} valc(D\_3) \texttt{&} \\}$$

Behauptung:

> (i) es existiert eine effektives Vorgehen zum Konstruieren eines E+A $D\_1$, und

--

<!-- .slide: class="nobull" -->


Beweis (i):

* Sei $D_3 = M$ eine beliebige TM

* $L'$ ist der Schnitt der 3 Sprachen:

* `$$\begin{align}
L_1 = & \{wx \mid w\in \texttt\$ valc(M) \texttt &, x\in (\texttt\$\Sigma^+\texttt&)^+\}
\\
L_2 = & \{w^n \mid w\in \texttt\$\Sigma^+\texttt&, n \geq 1 \}
\\
L_3 = & \{wx \mid w\in\texttt \$\Sigma^+\texttt &,x\in (\texttt \$\Sigma^+\texttt &)^+, |wx|_\texttt \$ = 2^{2^{|w|}} \}
\end{align}$$`

* Die von einem ZA erkennbaren Sprachen sind im Schnitt abgeschlossen (bew. in \[2\])

* $\implies$ Es reicht zu zeigen, dass die einzelnen Sprachen erkannt werden können

<small> [2] Klein, A., Kutrib, M.: Fast one-way cellular automata. Theoret. Comput. Sci. 295, 233–250
(2003)</small>

--

<!-- .slide: class="nobull" -->

`$$L_1 = \{wx \mid w\in \texttt\$ valc(M), x\in  (\texttt\$\Sigma^+\texttt&)^+\}$$`

* $valc(M)$ wird erkannt (Beweis in [1])
* E+A erkennen reguläre Sprachen
* E+A sind abgeschlossen unter Konkatenation (Beweis in [2])
* $\implies$ E+A erkennen $L_1$

<small> [1] Klein, A., Kutrib, M.: Cellular Automata: Descriptional Complexity and Decidability. Reversibility and Universality: Essays Presented to Kenichi Morita on the Occasion of his 70th Birthday
(2018)</small>


--

`$$L_2 = \{w^n \mid w\in \texttt\$\Sigma^+\texttt&, n \geq 1 \}$$`

* $L\_2$ ist im wesentlichen das Komplement einer linearen kontextfreien Sprache $L\_k$, welche von E+A erkannt werden können
* von E+A erkannte Sprachen sind unter Komplementbildung abgeschlossen.
* $\implies$ $L_2$ wird von E+A erkannt

<small>`$L_k \ni w_1 w_2 \dots w_n$` mit `$w_i \in  \texttt\$\Sigma^+\texttt&$` und `$1 ≤ j < k ≤ n$` mit `$w_j \neq w_k$`</small>

note:

<small>`$L_k \ni w_1\#w_2^{R}\#w_3\#\dots\#w_{n-1}^R \#w_n$` mit `$w_i \in  \texttt\$\Sigma^+\texttt&$` und `$1 ≤ j < k ≤ n$` mit `$w_j \neq w_k$`</small>


--

`$$L_3 = \{wx \mid w\in\texttt \$\Sigma^+\texttt &,x\in (\texttt \$\Sigma^+\texttt &)^+, |wx|_\texttt \$ = 2^{2^{|w|}} \}$$`

In [1, Lemma3] wird durch verwendung eine Zählvariable bewiesen, dass $L_3$ von E+A erkannt wird.


<small> [1] Klein, A., Kutrib, M.: Cellular Automata: Descriptional Complexity and Decidability. Reversibility and Universality: Essays Presented to Kenichi Morita on the Occasion of his 70th Birthday
(2018)</small>

--

Behauptung:

> (ii) $D\_1$ hat eine equivalente Beschreibung als EoA  genau dann wenn $D\_3$ nicht unendlich ist

Beweis: (ii)

* Wenn die $M$ endlich ist, dann hat ist $L(M)$ endlich. Alle endliche Sprachen können von EoA erkannt werden.
* Wenn die $M$ unendlich ist, dann kann sie nicht von EoA erkannt werden, wie in [3] bewiesen wurde mithilfe des Pumping Lemma für zyklische Zeichenketten

<small>[3] Nakamura, K.: Real-time language recognition by one-way and two-way cellular automata.
In: M. Kutyłowski, L. Pacholski, T. Wierzbicki (eds.) Mathematical Foundations of Computer
Science (MFCS 1999), LNCS, vol. 1672, pp. 220–230. Springer (1999)</small>

--

Damit sind alle vorraussetzung für das Theorem erfüllt, und die Behauptung bewiesen

> \[wenn] für eine beliebige Turingmaschine $D\_3$ gilt,

> (i) es existiert eine effektives Vorgehen zum Konstruieren eines E+A $D\_1$, und

> (ii) $D\_1$ hat eine equivalente Beschreibung als EoA  genau dann wenn $D\_3$ nicht unendlich ist

> dann ist der Trade-off zwischen E+A und EoA nicht berechenbar.

---

## Fragen?
