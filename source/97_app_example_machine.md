
## Konkrete Turingmaschine für Beispiele \label{tmex}

Für die Beispiele in dieser Arbeit wurde folgende Turingmaschine verwendet:\

Turingmaschine $M_b = \langle Q, \Sigma, T, \delta, q_0 , B, F \rangle$

* Zustand $Q = \{A,B,C\}$
* Eingabesymbole $\Sigma=\{0,1,2,3,4,5\}$,
* Bandsymbol $T=\Sigma\cup \{\square\}$,
* Startzustand $q_0=A$
* Blank $B=\square$
* Endzustand $F=C$
* Zustandsüberführungsfunktion $\delta$:
$$\begin{aligned}
(A,s)\mapsto &(A,0,r) (s \in \Sigma) \\
(A,\square)\mapsto &(B,0,l) \\
(B,s)\mapsto &(B,0,l) (s \in \Sigma) \\
(B,\square)\mapsto &(C,\square,r) \\
(C,t)\mapsto &(C,t,s) (t \in T)
\end{aligned}$$

Diese Turingmaschine überschreibt das gesamt Band mit "0", und bewegt dann den Lesekopf zurück auf die 1. Position des Wortes.

Die Python-Implementierung dieser Turingmaschine sieht wie folgt aus:

```python
class MyTuringMachine(object):
    states = ['A','B','C']
    symbols = ['0', '1','2','3','4','5', '□']
    blanc = '□'

    def transition(self, state, symbol):
        if state not in self.states:
            return 'C', '5', 's'
        if symbol not in self.symbols:
            return 'C', '4', 's'
        b = self.blanc
        if state == 'A':
            if symbol == b:
                return 'B', b, 'l'
            return 'A', '0', 'r'
        if state == 'B':
            if symbol == b:
                return 'C', b, 'r'
            return 'B', symbol, 'l'
        return state, symbol, 's'
```
