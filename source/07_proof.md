# Beweis von \Cref{t1} \label{proof}

Nun da wir alle Informationen haben, die wir brauchen, werden wir \Cref{t1} formal beweisen. Wir werden hierzu unter anderem $valc(M)$ betrachten, welches wir in \Cref{valc} beschrieben haben.

Zentraler Teil des Beweises ist \Cref{t2}. Es ist sehr mächtig und allgemein gehalten. Wir werden das Theorem auf unseren Kontext anwenden, indem wir die allgemeinen Konzepte durch unsere konkreten Konzepte ersetzen. Wir ersetzen folgende Wörter und Konzepte:

* $S_1 \rightarrow$ E+A
* $S_2 \rightarrow$ EoA
* $c_1 \rightarrow$ Anzahl Zustände
* $c_2 \rightarrow$ Anzahl Zustände
* $S_3 \rightarrow$ Menge der Turingmaschinen
* $D_3 \rightarrow$ beliebige Turingmaschinen
* $P \rightarrow$ Unendlichkeit der Sprache, die eine Turingmaschinen erkennt
* $D_1 \rightarrow$ E+A, der $L_c$ erkennt
* $D_2 \rightarrow$ EoA, der $L_c$ erkennt

Damit erhalten wir folgendes Resultat (Die ersetzten Konzept sind hervorgehoben):

\Begin{proposition}\label[proposition]{t2a}
Seien **E+A** und **EoA** zwei Beschreibungssysteme für berechenbare Sprachen, so dass beliebige Beschreibungen $D$ in **E+A** und **EoA** effektiv in eine Turingmaschine konvertiert werden können, die $L(D)$ beschreibt, und sei **Anzahl Zustände** ein Maß für **E+A** und **Anzahl Zustände** ein sn-Maß für **EoA**. Wenn es ein Beschreibungssystem **Turingmaschinen** gibt mit einer Eigenschaft **Unendlichkeit**, die nicht semi-entscheidbar ist für Beschreibungen von Turingmaschinen, sodass für ein beliebige **Turingmaschine** $D_3$ in **der Menge der Turingmaschinen** gilt,

(i) es existiert ein effektives Vorgehen zum Konstruieren einer Beschreibung $D_1$ als **E+A**, und

(ii) $D_1$ hat eine äquivalente Beschreibung als **EoA** genau dann wenn $D_3$ **nicht unendlich ist**,

dann ist der Trade-off zwischen **E+A** und **EoA** nicht berechenbar.
\End{proposition}

Der erste Teil von \Cref{t2a} ist trivialerweise wahr. Es bleibt zu zeigen:


\Begin{proposition}\label[proposition]{t2b}
\[wenn] für eine beliebige Turingmaschine $D_3$ gilt,

(i) es existiert ein effektives Vorgehen zum Konstruieren eines E+A $D_1$, und
(ii) $D_1$ hat eine äquivalente Beschreibung als EoA  genau dann wenn $D_3$ nicht unendlich ist,

dann ist der Trade-off zwischen E+A und EoA nicht berechenbar.
\End{proposition}

Wir brauchen also nur (i) und (ii) zeigen, um \Cref{t1} beweisen.\

Für den Beweis muss der E+A $D_1$ abhängig sein von der TM $D_3$. Wir wählen $D_1$ mit

$$L' = L(D_1) =  \{ w^{2^{2^{|w|}}}\mid w \in \texttt{\$} valc(D_3) \texttt{\&} \}$$

$L(D)$ ist die Sprache, die durch eine Beschreibung $D$ definiert wird. $D_3 = M$ sei eine beliebige Turingmaschine. Wir zeigen, dass (i) für $D_1$ gilt:

$L'$ ist der Schnitt der 3 Sprachen:

$$\begin{aligned}
L_1 = & \{wx \mid w\in \texttt\$ valc(M) \texttt \&, x\in (\texttt\$\Sigma^+\texttt\&)^+\}
\\
L_2 = & \{w^n \mid w\in \texttt\$\Sigma^+\texttt\&, n \geq 1 \}
\\
L_3 = & \{wx \mid w\in\texttt \$\Sigma^+\texttt \&,x\in (\texttt \$\Sigma^+\texttt \&)^+, |wx|_\texttt \$ = 2^{2^{|w|}} \}
\end{aligned}$$

Da die von E+A erkennbaren Sprachen im Schnitt abgeschlossen sind (Beweis in [@fast]), reicht es zu zeigen, dass jede dieser 3 Sprachen von einem E+A erkannt werden kann.

In [@ais] wurde bewiesen, dass die Sprache $valc(M)$ von E+A erkannt werden können. Zudem werden reguläre Sprachen von E+A erkannt. Da die von E+A erkannten Sprachen unter Konkatenation abgeschlossen sind (Beweis in [@fast]), kann somit $L_1$ von E+A erkannt werden.

$L_2$ ist im wesentlichen das Komplement einer linearen kontextfreien Sprache $L_k$ mit $L_k \ni w_1 w_2 \dots w_n$ mit $w_i \in  \texttt\$\Sigma^+\texttt\&$ und $1 \leq j < k \leq n$ mit $w_j \neq w_k$, welche von E+A erkannt werden kann. Die von E+A erkannte Sprachen sind unter Komplementbildung abgeschlossen. Daher kann $L_2$ von E+A erkannt werden.

In [@ais, Lemma3] wird mithilfe einer Zählvariablen bewiesen, dass $L_3$ von E+A erkannt werden können. \

Es bleibt zu zeigen, dass (ii) für $D_1$ gilt. Fall 1: $D_3$ erkennt nur eine endliche Sprache, dann erkennt $D_1$ auch nur eine endliche Sprache. Endliche Sprachen können trivialerweise von EoA erkannt werden. Fall 2: $D_3$ erkennt unendliche Sprachen. Dann existiert kein zu $D_1$ äquivalenter EoA, wie in [@naka] mithilfe des Pumping Lemma für zyklische Zeichenketten bewiesen wurde. \

Damit ist gezeigt, dass \Cref{t2b} wahr ist, was zusammen mit \Cref{t2}  \Cref{t1} beweist.  \qed

\clearpage
