
## Implementierung eines EoA, der valc(M) erkennt in python \label{implement}

Dies ist die Implementierung des Zellularautomaten, der in \Cref{valc} beschrieben wurde. \footnote{Ein Jupyter-notebook, dass diese Implementierung verwendet befindet sich unter \url{https://gitlab.com/juergens/sem_ca/blob/master/notebook.ipynb}}

Diese Implementierung ist nicht vollständig, da nicht alle Randfälle, die einen Fehler werfen sollten, auch tatsächlich einen Fehler werfen.  Außerdem ist diese Implementierung nicht wirklich lesbar. Es wurden keine sinnvollen Namen vergeben. Methoden und Objekte wurden nicht sinnvoll verwendet. Es wurde keine Form von Design-Pattern oder Richtlinien angewendet. Außerdem wurde in \Cref{valc} ein 6. Teil des Tupels beschrieben, in dem sich gemerkt wurde, ob ein Endzustand gelesen wurde; Dieser Teil des Tupels ist hier nicht implementiert.

Die Zustandsüberführung wird ausgeführt durch einen Aufruf der Funktion `valc_ca`. Die Klasse `Cell` dient zur Vereinfachung von Implementierung inerhalb von `valc_ca` und hat keine Bedeutung außerhalb dieser Funktion.

Die Implementierung sollte auch mit jeder anderen Turingmaschine funktionieren, solange die notwendigen Member-Variablen vorhanden sind, und solange die Anforderungen aus \Cref{valc} erfüllt sind.


```python
class Cell(object):
    def __init__(self, o, n):
        self.o = o
        self.n = n
        s = [o[0], n[1], o[1], '_', o[4],color_todo]
        self.state = s
        if n[1] == '_':
            s[1] = n[0]

    def s(self,value,color):
        self.state[3] = value
        self.state[5] = color
        return self.state



def valc_ca(old_cell_state, old_neighbor_state):
    tm = MyTuringMachine()
    o = old_cell_state
    n = old_neighbor_state
    cell = Cell(o,n)
    s = cell.state


    ######################
    ## set first-cell info
    ######################
    # order is important here, because no return
    # sets the 4th value, which is only relevant for the first cell

    if o[4] == '1':
        if o[3] =='Y' or  o[3] == 'V':
            s[4] = '0'
        elif o[3] == 'X':
            s[4] = 'F'
    elif o[4] == '0':
        if o[3] == 'X' or o[3] == 'V':
            s[4] = '1'
            #return cell.s('F', color_error)
        elif o[3] =='Y':
            s[4] = 'F'
    elif o[4] == '_':
        if o[3] == 'X':
            # signal to right-move received.
            s[4] = '1'
        elif o[3] == 'Y':
            s[4] = 'F'

    ####################
    ## stuff for first iteration
    #####################

    if o[1] == '_':
        s[2] = o[0]
        if n[0] =='#':
            return cell.s('*', color_first_cross)

    #######################
    ## error handling
    #######################
    if n[3] == 'F':
        return cell.s('F',color_error)
    if o[3] == 'F':
        return cell.s('F',color_error)
    if n[3] == '?':
        return cell.s('?',color_todo)
    if o[3] == '?':
        return cell.s('?',color_todo)


    #######################
    ## interesting stuff
    #######################

    if o[3] == '*':
        if n[3] == '_':
            # end of stars is reached
            if o[0] == n[1]:
                # successful copy
                return cell.s('C',color_first_copy)

            if o[1] == '#' and o[0] in tm.states:
                # head has reached first blanc on right side
                return cell.s('N',color_tm_waiting)

            if n[1] in tm.states:
                # a right move must follow
                return cell.s('P',color_tm_waiting)
            else:
                # a left move must follow
                return cell.s('M',color_tm_waiting)

    if n[3] == 'N':
        # head has reached word end before, so now must follow a copy
        if o[0] == n[1]:
            # successful copy
            return cell.s('C',color_first_copy)
        else:
            return cell.s('F', color_error)

    if o[3] == 'N':
        # the head was at the end, now we check if it moved correct.
        next_tm_state = n[1]
        last_tm_state= o[0]
        last_symbol = tm.blanc
        next_symbol = tm.blanc

        expected_state, expected_symbol, expected_move = tm.transition(last_tm_state, last_symbol)
        # print("state", last_tm_state, next_tm_state, expected_state)
        # print(last_symbol, next_symbol, expected_symbol, expected_move)
        if next_tm_state == expected_state and next_symbol == expected_symbol and expected_move =='l':
            return cell.s('L', color_tm_correct_left)
        return cell.s('F', color_error)

    if n[3] == 'L':
        return cell.s('H',color_tm_waiting)
    if o[3] == 'L':
        return cell.s('_',color_boring)

    if n[3] == 'P':
        return cell.s('Q',color_tm_waiting)
    if o[3] == 'P':
        return cell.s('_',color_boring)
    if o[3] == 'M':
        return cell.s('_',color_boring)

    if n[3] == 'Q':
        return cell.s('*',color_done)

    if n[3] == 'M':
        return cell.s('K',color_tm_waiting)
    if n[3] == 'K':
        return cell.s('*',color_done)

    if o[3] == 'K':
        return cell.s('I',color_tm_waiting)

    if n[3] == 'I':
        if o[0] == n[1]:
            # successful copy
            return cell.s('*',color_first_copy)
        return cell.s('U',color_first_copy)

    if o[3] == 'U':
        if n[3] == 'J':
            # this is fine after transition, but not everywhere else
            # but it must also be a copy
            if o[0] == n[1]:
                return cell.s('Y',color_done_invert_state)
        return cell.s('F', color_error)

    if n[3] == 'J' and o[3] != 'U':
        # dunno... prolly happens after the head hits the a blanc on the right side in a inverted sub word?
        return cell.s('Y',color_done_invert_state)

    if o[3] == 'J' and n[3] == '_':
        # prolly fine. not sure
        return cell.s('_',color_boring)

    if n[3] == 'U':
        return cell.s('*',color_done)

    if n[3] =='Y':
        return cell.s('Y',color_done_invert_state)

    if o[3] == 'Y' and n[3] == '_':
        return cell.s('Z',color_done)

    if o[3] == 'Z':
        return cell.s('_',color_boring)
    if n[3] == 'Z':
        if o[0] == n[1]:
            return cell.s('C',color_first_copy)
    if o[3] == 'I':
        next_tm_state = n[2]
        last_tm_state= o[1]
        last_symbol = o[0]
        next_symbol = o[2]

        expected_state, expected_symbol, expected_move = tm.transition(last_tm_state, last_symbol)
        if next_tm_state == expected_state and next_symbol == expected_symbol:
            if expected_move =='l':
                # move found in right direction
                return cell.s('L', color_tm_correct_right)
            # move found in inverted direction
            return cell.s('J', color_tm_correct_right)
        return cell.s('F', color_error)

    if o[3] == 'Q':
        last_tm_state = o[0]
        next_tm_state = o[1]
        last_symbol = n[0]
        next_symbol = n[1]

        expected_state, expected_symbol, expected_move = tm.transition(last_tm_state, last_symbol)

        if next_tm_state == expected_state and next_symbol == expected_symbol:
            if expected_move =='r':
                return cell.s('R', color_tm_correct_right)
            # maybe this is the case, where both directions may have worked?
            return cell.s('O', color_tm_correct_both)

        # maybe this at the end of a thing, and also inverted?
        last_symbol = tm.blanc
        next_symbol = tm.blanc
        expected_state, expected_symbol, expected_move = tm.transition(last_tm_state, last_symbol)

        # print("state:" + last_tm_state + next_tm_state + expected_state)
        # print("symbo:" + last_symbol + next_symbol + expected_symbol)

        if next_tm_state == expected_state and next_symbol == expected_symbol:
            if expected_move =='l':
                return cell.s('J', color_tm_correct_right)
            # this maybe the case when hitting the blanc on the left in a not-inverted subword
            return cell.s('R', color_tm_correct_right)

        return cell.s('F', color_error)

        return cell.s('F', color_error)
    if n[3] == 'O':
        return cell.s('V', color_tm_correct_both_signal)
    if o[3] == 'O':
        # todo: here we need to take care of both possiblities.
        # for now just assume it was actually a right move
        return cell.s('_', color_boring)

        return cell.s('_',color_todo)

    if n[3] == 'V':
        return cell.s('V', color_tm_correct_both_signal)
    if o[3] == 'V':
        return cell.s('*',color_done)

    if n[3] == 'R':
        return cell.s('X',color_done_normal_state)
    if n[3] == 'X':
        return cell.s('X',color_done_normal_state)

    if o[3] == 'X':
        return cell.s('*',color_done)
    if n[3] == 'H':
        return cell.s('G',color_tm_waiting)
    if o[3] == 'H':
        return cell.s('_',color_boring)

    if n[3] == 'G':
        return cell.s('X',color_done_normal_state)
    if o[3] == 'G':
        if o[0] == n[1]:
            # first copy after tm-left move
            return cell.s('C', color_first_copy)
        else:
            return cell.s('F', color_error)
    if o[3] == 'R':
        return cell.s('_', color_boring)

    ########################
    # regular boring stuff
    ########################
    if n[3] == '*':
        return cell.s('*',color_done)
    if o[3] == 'C':
        return cell.s('_', color_boring)
    if n[3] == 'C':
        return cell.s('*',color_done)
    if o[3] == '_' and n[3] == '_':
        return cell.s('_', color_boring)

    return cell.s('?',color_todo)
```
