
# Einleitung

\blfootnote{Den Quelltext dieser Arbeit befindet sich unter \url{https://gitlab.com/juergens/sem_ca}}

In der Beschreibungskomplexität (engl. _Descriptional Complexity_) werden Berechnungsmodelle daraufhin untersucht, wie effizient formale Sprachen mit ihnen beschrieben werden können. Unter anderem vergleicht man die Länge von Kodierungen von Sprachen in einem Berechnungsmodell mit Kodierungen der selben Sprachen in anderen Modellen. Bei dem Konvertieren einer Sprache von einem Modell in ein anderes, kann die Länge der Beschreibung so stark anwachsen, dass sie durch keine berechenbare Funktion begrenzt ist. In diesem Fall sprechen wir von einem nicht-berechenbaren Trade-off (engl. _non recursive trade-off_).


Zellularautomaten (engl. _celluar automata_) sind ein Berechnungsmodell mit vielen Varianten und möglichen Beschränkungen. Eine Variante sind die Einwegzellularautomaten (engl. _one way cellular automata_), in der Zellen nur in eine Richtung Informationen weitergeben. Man kann Einwegzellularautomaten weiter beschränken, in dem man die Rechenzeit begrenzt. Wir sprechen von Echtzeit Einwegzellularautomaten (im Folgendem abgekürzt mit "EoA"), wenn die Berechnung einer Eingabe der Länge $n$ nach $n$ Schritten beendet ist. Wir sprechen von Echtzeit plus log Einwegzellularautomaten (im Folgendem abgekürzt mit "E+A"), wenn die Berechnung erst nach $n + log (n)$ Schritten endet.

Die minimale Anzahl von Zuständen, die ein EoA benötigt um die Sprache $L$ zu erkennen sei $s_o(L)$. Analog sei $s_+(L)$ die minimale Anzahl an Zuständen, die ein E+A benötigt. Wir sagen $s(L) = \infty$ genau dann wenn $L$ nicht von dem Beschreibungsmodell erkannt werden kann\footnote{Es sei darauf hingewiesen, dass es auch andere Metriken in der Beschreibungskomplexität gibt, wie z.B. "Länge der Kodierung des Deskriptors" oder "Anzahl Regeln in der Zustandsüberführungsfunktion". Im Rahmen dieser Arbeit spielt diese Unterscheidung jedoch keine Rolle, denn wenn eine dieser Metriken unberechenbar ist, dann sind die anderen es in der Regel auch nicht berechnenbar. }. Sei $\mathbb F$ die Menge der berechenbaren Funktionen auf $\mathbb N$.\


\begin{figure}
\centering
\subfloat[]
{
    \includegraphics[width=0.33\textwidth]{assets/ca2.eps}
    \label{fig:ca}
}
\hspace{0.08\textwidth}
\subfloat[]
{
    \includegraphics[width=0.33\textwidth]{assets/oca2.eps}
    \label{fig:oca}
}
\caption{ \protect\subref{fig:ca} In einem normalen eindimensionalen Zellularautomaten können sich Informationen in alle Richtungen bewegen. \protect\subref{fig:oca} In einem Einwegzellularautomaten können die Informationen nur in eine Richtung wandern.}
\label{fig:caoca}
\end{figure}



In dieser Arbeit werden wir folgendes Theorem beweisen:


> \Begin{theorem}\label[theorem]{t1}[@ais]
> Der Trade-off zwischen E+A und EoA ist nicht berechenbar.
> \End{theorem}


Diese Behauptung ist äquivalent zu

> Die Einsparung an Zuständen, die sich beim Wechseln von EoA zu E+A ergeben können, ist nicht durch eine berechenbare Funkion begrenzt.

Dies ist wiederum äquivalent ist zu:

> für alle berechenbaren Funktionen $f\in\mathbb F$ gilt:
>
> $$\exists L:f(s_+(L)) < s_o(L)$$

Hierzu werden wir einen E+A konstruieren, der die Sprache der validen Berechnungen einer Turingmaschine erkennt. Diese Sprache werden wir verwenden, um eine weitere Sprache zu konstruieren, die effizient von E+A erkannt werden kann, nicht jedoch von EoA. Diese Sprache werden wir dann zusammen mit \Cref{t2} verwenden, um \Cref{t1} zu beweisen.


> \Begin{theorem}\label[theorem]{t2}[@ais]
>
> Seien $S_1$ und $S_2$ zwei Beschreibungssysteme für berechenbare Sprachen, so dass beliebige Beschreibungen $D$ in $S_1$ und $S_2$ effektiv in eine Turingmaschine konvertiert werden können, die $L(D)$ beschreibt, und sei $c_1$ ein Maß für $S_1$ und $c_2$ ein sn-Maß für $S_2$. Wenn es ein Beschreibungssystem $S_3$ gibt mit einer Eigenschaft $P$, die nicht semi-entscheidbar ist für Beschreibungen von $S_3$, sodass für ein beliebiges $D_3$ in $S_3$ gilt,
>
> (i) existiert eine effektives Vorgehen zum Konstruieren einer Beschreibung $D_1$ in $S_1$, und
>
> (ii) $D_1$ hat eine äquivalente Beschreibung in $S_2$  genau dann wenn $D_3$ keine Eigenschaft $P$ hat,
>
> dann ist der Trade-off zwischen $S_1$ und $S_2$ nicht berechenbar.
> \End{theorem}


## Begriffe und Annahmen

Im Rahmen dieser Arbeit werden Sprachen als gleich angesehen, wenn sie sich höchstens im leeren Wort unterscheiden. Dies erspart Schreibarbeit.

Wenn wir die Komplexität zweier Systeme vergleichen, wird angenommen, dass der Schnitt der durch ihnen beschrieben Sprachen nicht leer ist.

Wir verwenden Schreibweisen für Mengen von Wörtern, die teilweise explizite Symbole verwenden, wie z.B. $w \in (\texttt \$\Sigma^+ \texttt \& )$. Dies bedeutet, dass das Wort $w$ mit einem \texttt \$ beginnt, beliebig viele Symbole aus der Menge $\Sigma$ enthält und mit einem \texttt \& endet.

Ein ausführliches Glossar befindet sich in \Cref{glossar}.

## Motivation und Weiterführende Arbeiten

Es ist überraschend, dass ein so kleiner Unterschied wie "$log(n)$ zusätzliche Schritte" einen so massiven Trade-off ermöglicht. Es ist kaum möglich, sich einen stärkeren Trade-off vorzustellen als einen unberechenbaren Trade-off. Der allgemeine Nutzen der Betrachtung von Beschreibungskomplexität besteht darin, dass es uns ermöglicht Aussagen darüber zu machen, wie effizient man in einem Beschreibungssystem eine Implementierung umsetzen kann. Zudem hilft es uns Aussagen über die Grenzen von Beschreibungssysteme zu finden machen.

Im Allgemeinen sind Zellularautomaten interessant, da sie eine alternative Sicht auf die Welt ermöglichen [@Wolfram2002]. Zellularautomaten als Hardware sind sehr interessant, da sie im Gegensatz zu herkömmlichen Berechnungsmodellen sehr viel einfacher strukturiert sind und gleichzeitig parallelisierbare Probleme sehr viel schneller lösen können [@hardware]. Daher ist es naheliegend, die Struktur noch weiter zu vereinfachen, in dem wir bidirektionalen Datenaustausch unterbinden und Einbahnzellularautomaten betrachten. Da der andere große Vorteil von Zellularautomaten in ihrer Parallelisierbarkeit, und damit in ihrer Geschwindigkeit besteht, ist es außerdem naheliegend, die Einschränkung der Echtzeit (beziehungsweise Echtzeit plus log) zu betrachten. Für eine reale Implementierung eines Algorithmus ist zudem relevant, wie komplex das Programm ist, das diesen Algorithmus ausführt, da dies z.B. Einfluss auf die notwendige Größe des Programmspeicher hat. Daher ist eine Betrachtung der Komplexität auch naheliegend. \

Unberechenbare Trade-offs zwischen Berechnungsmodellen wurden zuerst 1971 beobachtet[@economy], wo insbesondere die Größe von endlichen Automaten und Kontextfreien Grammatiken verglichen wurde. Seitdem wurde dieses Phänomen "nahezu überall" beobachtet. Für eine Einführung in das Gebiet der unberechenbaren Trade-offs empfiehlt sich \textit{Non-Recursive Trade-Offs Are ``Almost Everywhere''}[@everywhere]. Eine allgemeine Einführung in Trade-offs findet man in \textit{Descriptional Complexity — An Introductory Survey}[@ais]. Hier werden nicht nur unberechenbare und berechenbare Trade-offs berücksichtigt, sondern auch Trade-offs zwischen Zellularautomaten und anderen Berechnungsmodellen. Aus diesem Paper stammt insbesondere \Cref{t2}, was ein mächtiges Werkzeug für Beweise von unberechenbaren Trade-offs darstellt. Die erste Variante dieses Theorems wurde in einer anderen Arbeit  bewiesen [@pheno].

Eine Reihe ausführlicher Definitionen im Rahmen des Phänomens der unberechenbaren Trade-offs finden Sie in
in \textit{On Measuring Non-Recursive Trade-Offs} @nt. Das Phänomen wird hier sehr konkret und formal beschrieben, weshalb diese Arbeit eine Grundlage für viele formale Betrachtungen des Problems dar stellt.

In \textit{Cellular Automata: Descriptional Complexity and Decidability}[@ca] wird gezielt auf unberechenbare Trade-offs zwischen Zellularautomaten eingegangen. Diese Paper stellt somit den Kern dieser Seminararbeit dar.


\clearpage
